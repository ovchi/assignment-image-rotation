#pragma once
#include "structures.h"
#include <stdint.h>

void image_clear(struct image* image_to_clear);

struct image rotate(struct image const source);

