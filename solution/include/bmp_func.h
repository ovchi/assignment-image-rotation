#include "image_func.h"
#include "statuses.h"
#include <stdint.h>
#include <stdio.h>

enum read_status header_checker(struct bmp_header* h);

struct bmp_header bmp_shell(void);

uint32_t calc_padding(uint64_t w);

void get_bmp_attributes(uint64_t h, uint64_t w, uint32_t* file_s, uint32_t* img_s, uint32_t* p);

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);

