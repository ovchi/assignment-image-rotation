#include "../include/bmp_func.h"
#include <stdlib.h>

enum read_status header_checker(struct bmp_header* h){
    if(h->bfType != 0x4d42){
        return READ_INVALID_SIGNATURE;
    }
    if(h->bOffBits != sizeof(struct bmp_header)){
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

struct bmp_header bmp_shell(void){
    struct bmp_header new_header = {
            .biWidth = 0,
            .biHeight = 0,
            .bfileSize = 0,
            .biSizeImage = 0,
            .bfType = 0x4d42,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return new_header;
}

uint32_t calc_padding(uint64_t w){
    return(4 - ((sizeof(struct pixel) * w) % 4));
}

void get_bmp_attributes(uint64_t h, uint64_t w, uint32_t* file_s, uint32_t* img_s, uint32_t* p){
    *p = calc_padding(w);
    *file_s = (w * h * sizeof(struct pixel) + h * *p) * sizeof(struct bmp_header);
    *img_s = (w * h * sizeof(struct pixel) + h * *p);
}

enum read_status from_bmp(FILE* in, struct image* img ) {
    struct bmp_header header_old;
    if (fread(&header_old, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header_checker(&header_old) != READ_OK) {
        return header_checker(&header_old);
    }

    *img = (struct image){.width=header_old.biWidth, .height=header_old.biHeight};
    img->data =  (struct pixel*)malloc(header_old.biHeight*header_old.biWidth*sizeof(struct pixel));
    if (img->data == NULL){
        return READ_ERROR;
    }
    uint32_t padding_img = calc_padding(img->width);
    for (size_t y = 0; y < img->height; ++y) {
        size_t pixels_read = fread(&(img->data[y * img->width]), sizeof(struct pixel), img->width, in);
        if (pixels_read != img->width) {
            return READ_INVALID_HEADER;
        }
        if (padding_img > 0 && fseek(in, padding_img, SEEK_CUR) != 0) {
            return READ_INVALID_HEADER;
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){

    struct bmp_header new_header = bmp_shell();
    uint32_t file_size = 0;
    uint32_t img_size = 0;
    uint32_t padding = 0;
    get_bmp_attributes(img->height, img->width, &file_size, &img_size, &padding);
    new_header.biWidth = img->width;
    new_header.biHeight = img->height;
    new_header.bfileSize = file_size;
    new_header.biSizeImage = img_size;

    if (fwrite(&new_header, sizeof(new_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    for (size_t row = 0; row < img->height; ++row) {
        if (fwrite(img->data + row * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }else{
            char pad_byte = 0;
            for (size_t pad = 0; pad < padding; ++pad) {
                if (fwrite(&pad_byte, 1, 1, out) != 1) {
                    return WRITE_ERROR;
                }
            }
        }
    }
    free(img -> data);
    return WRITE_OK;
}
