#include "../include/bmp_func.h"
#include <stdlib.h>

void read_statuses_writer(enum read_status rs){
    if (rs == READ_OK){
        fprintf(stderr, "%s", "read ok");
    }else if(rs == READ_INVALID_BITS){
        fprintf(stderr, "%s", "invalid bits");
    }else if(rs == READ_INVALID_HEADER){
        fprintf(stderr, "%s", "invalid header");
    }else if(rs == READ_INVALID_SIGNATURE){
        fprintf(stderr, "%s", "invalid signature");
    }
}

int main( int argc, char** argv ) {
    if (argc != 3){
        fprintf(stderr, "%s", "not enough params");
        return 0;
    } else{
        FILE* get_file = fopen(argv[1], "rb");
        FILE* result_file = fopen(argv[2], "wb");
        if (get_file == NULL || result_file == NULL){
            fclose(get_file);
            fclose(result_file);
            fprintf(stderr, "%s", "can't open file");
            return 0;
        }else{
            struct image img = {0};
            enum read_status status_r = from_bmp(get_file, &img);
            if (status_r != READ_OK){
                read_statuses_writer(status_r);
                fclose(get_file);
                fclose(result_file);
                return 0;
            }else{
                struct image result_image = rotate(img);
                enum write_status status_w = to_bmp(result_file, &result_image);
                if (status_w != WRITE_OK){
                    fprintf(stderr, "%s", "write error");
                    fclose(get_file);
                    fclose(result_file);
                    return 0;
                }else{
                    fprintf(stderr, "%s", "write ok");
                    fclose(get_file);
                    fclose(result_file);
                }
            }
        }
    }
    fprintf(stderr, "%s", "all done");
    return EXIT_SUCCESS;
}
