#include "../include/image_func.h"
#include <stdlib.h>

void image_clear(struct image* image_to_clear){
    free(image_to_clear->data);
}

struct image rotate( struct image const source ){
    struct image image_to_rotate = (struct image){.width=source.height, .height=source.width};
    image_to_rotate.height = source.width;
    image_to_rotate.width = source.height;
    image_to_rotate.data =  malloc(source.width*source.height*sizeof(struct pixel));
    if (image_to_rotate.data == NULL){
        return source;
    }
    for (uint64_t par_y = 0; par_y < source.height; par_y++) {
        for (uint64_t par_x = 0; par_x < source.width; par_x++) {
           image_to_rotate.data[par_x * image_to_rotate.width + (source.height - par_y - 1)] = source.data[par_y * source.width + par_x];
        }
    }
    free(source.data);
    return image_to_rotate;
}
